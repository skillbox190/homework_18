
#include <iostream>

class Stack
{
private:
    
    int* arr;
    int top;
    int capacity;

public:

    Stack(int size)
    {
        arr = new int[size];
        capacity = size;
        top = -1;
    }

    void Push(int x)
    {
        std::cout << x << '\n';
        arr[++top] = x;
    }
    
    int Pop()
    {
        std::cout << peek() << '\n';
        return arr[top--];
    }

    int peek()
    {
        return arr[top];
    }
};

int main()
{
    Stack v1(5);
    v1.Push(1);
    v1.Push(2);
    v1.Push(3);
    v1.Push(4);
    v1.Push(5);
    v1.Pop();
    v1.Pop();
}

